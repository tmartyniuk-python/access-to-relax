import argparse
from pprint import pformat, pprint
from os.path import basename, splitext
import re


def main():
    # parse args
    parser = argparse.ArgumentParser()
    parser.add_argument('out', type=str, help='file to write result to, created if doesnt exist')
    parser.add_argument('tfiles', metavar='filename', type=str, nargs='+',
                        help='filenames of exported access tables')

    args = parser.parse_args()

    tables = []
    for table_file in args.tfiles:
        try:
            tables.append(construct_table(table_file))
            pass
        except Exception as e:
            print(f'Error parsing file \'{table_file}\' : {e}')

    relax_db_name = f'group: {filename_no_extention(args.out)}'
    tables.insert(0, relax_db_name)
    
    translated_db_def = translit('\n\n'.join(tables))
    with open(args.out,  mode='w', encoding='utf-8') as out_file:
        out_file.write(translated_db_def)


def construct_table(exported_table_file):
    '''
        converts the table exported from access to its RelaX definition
        table is named by its filename
        :param exported_table: table filename
        :returns: string defining the access table in RelaX
    '''
    col_names, rows = parse_exported_table(exported_table_file)

    res = []
    table_name = filename_no_extention(exported_table_file)

    # table definition open
    res.append(f'{table_name} = {{')
    res.append(f'\t{ ", ".join(col_names) }')

    # relax expects strings to be enclosed in ''
    for row in rows:
        for i in range(len(row)):
            if row[i].isdigit():
                continue

            if not row[i]:
                row[i] = 'null'
            else:
                row[i] = f'\'{ row[i]}\''

    row_strs = [f'\t{", ".join(row)}' for row in rows]
    res.extend(row_strs)
    res.append('}')

    return '\n'.join(res)


def parse_exported_table(exported_table_file):
    '''
        parses column defs and rows from exported table file
        :returns: tuple(columns : list of strings, rows : list of strings)
    '''
    with open(exported_table_file, mode='r', encoding='utf-8') as file:
        exported_table = file.read().split('\n')

    # parse column names
    # 1st line skipped
    columns_line = 1
    col_names = get_line_values(exported_table[columns_line])

    rows = []
    # skipping the lines with -----------------
    for i in range(columns_line + 2, len(exported_table) - 1, 2):
        row = get_line_values(exported_table[i])
        assert len(row) == len(col_names), f"row #{i} has {len(row)} values, but there are only {len(col_names)} columns"
        rows.append(row)

    # empty line at EOF
    rows.pop(len(rows) - 1)
    return (col_names, rows)


def get_line_values(line):
    '''
        parses exported table file line into an array of values (column names or values),
        removing the | and whitespaces 
    '''
    values_re = re.compile('\s*(.*?)\s*\|')
    values = values_re.findall(line)

    # first is always an empty string, dont know why)
    values.pop(0)
    return values


def translit(text):
    symbols = (u"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯіІ",
           u"abvgdeejzijklmnoprstufhzcss_y_euaABVGDEEJZIJKLMNOPRSTUFHZCSS_Y_EUAiI")

    translate_table = {ord(a): ord(b) for a, b in zip(*symbols)}
    return text.translate(translate_table)


def filename_no_extention(filename):
    return splitext(basename(filename))[0]


if __name__ == "__main__":
    main()
